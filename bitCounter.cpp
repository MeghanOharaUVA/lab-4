// Meghan O'Hara mmo2bf 9/21/2016 bitCounter.cpp

#include <iostream>
#include <math.h>
#include <stdlib.h>
using namespace std;



int numNs(int n) {
   if (n == 0) {
      return 0;
    }

   else if (n == 1) {
      return 1;
    }
    
   else {
     int newN = (n / 2); 
     int newN2 = (n % 2); 
     return numNs(newN) + numNs(newN2); 
    }
}
  

int main(int argc, char **argv) {
  int x = atoi(argv[1]); 
  cout << numNs(x) << endl; 
}
