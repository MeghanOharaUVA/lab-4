//Meghan O'Hara mmo2bf 9/20/2016 inlab4.cpp

#include <iostream>
#include <climits>
#include <float.h>
using namespace std;


  char c0 = '0';
  char c1 = '1'; 
  int i0 = 0;
  int i1 = 1; 
  float f0 = 0.0;
  float f1 = 1.0; 
  unsigned int ui0 = 0;
  unsigned int ui1 = 1; 
  bool bT = true;
  bool bF = false; 
  double d0 = 0.0;
  double d1 = 1.0; 
  int* ip = NULL; 
  char* cp = NULL; 
  double* dp = NULL;


int main() {


  cout << "c0 " << c0 << endl;
  cout << "c1 " << c1 << endl;
  cout << "i0 " << i0 << endl;
  cout << "i1 " << i1 << endl;
  cout << "f0 " << f0 << endl;
  cout << "f1 " << f1 << endl;
  cout << "ui0 " << ui0 << endl;
  cout << "ui1 " << ui1 << endl;
  cout << "bT " << bT << endl;
  cout << "bF " << bF << endl;
  cout << "d0 " << d0 << endl;
  cout << "d1 " << d1 << endl;
  cout << "ip " << ip << endl;
  cout << "dp " << dp << endl; 

  // ----------------------------------------
  cout << "\nNext section (Primitve Arrays)" << endl;

  int IntArray[10] =
    {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
  char CharArray[10] =
    {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'}; 
 

  int IntArray2D[6][5] =
    { {1, 2, 3, 4, 5}, {11, 12, 13, 14, 15}, {21, 22, 23, 24, 25}, {31, 32, 33, 34, 35}, {41, 42, 43, 44, 45},  {51, 52, 53, 54, 55} }; 
  char CharArray2D[6][5] =
    { {'1', '2', '3', '4', '5'}, {'1', '2', '3', '4', '5'}, {'1', '2', '3', '4', '5'}, {'1', '2', '3', '4', '5'}, {'1', '2', '3', '4', '5'}, {'1', '2', '3', '4', '5'} }; 

 
}
