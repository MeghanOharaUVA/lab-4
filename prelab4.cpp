// Meghan O'Hara mmo2bf 9/18/16 prelab4.cpp

#include <iostream>
#include <climits>
#include <math.h>
using namespace std; 

void sizeOfTest() {

cout << "Size of int: " << sizeof(int) << endl;
cout << "Size of unsigned int: " << sizeof(unsigned int) << endl; 
cout << "Size of float: " << sizeof(float) << endl;
cout << "Size of double: " << sizeof(double) << endl;
cout << "Size of char: " << sizeof(char) << endl;
cout << "Size of bool: " << sizeof(bool) << endl;
cout << "Size of int*: " << sizeof(int*) << endl;
cout << "Size of char*: " << sizeof(char*) << endl;
cout << "Size of double*: " << sizeof(double*) << endl; 

}


void outputBinary(unsigned int x) {

  unsigned int temp = x;

  for (int i = 31; i >= 0; i--) {
    unsigned int y = pow(2, i); 
    if (temp >= y) {
      cout << 1;
      temp = temp - y; 
    }

    else {
      cout << 0; 
    }; 
  }

  cout << endl; 
  
} 

void overflow() {
  unsigned int x = UINT_MAX;
  //cout << x << endl; 
  unsigned int y = x + 1;
  cout << "Overflow value: " << y << endl;

  cout << "Because unsigned int x is defined as being the max possible value for an unsigned int, then it is not possible for the program to assign an unsigned int y as x + 1.  This number simply cannot be defined since it is above the pre-established max. Instead, it returns 0." << endl; 
}

int main() {
  
 sizeOfTest();
 unsigned int inp; 
 cout << "Enter a number to be converted to binary: "; 
 cin >> inp;
 cout << "Binary representation of " << inp << ":" << endl; 
 outputBinary(inp);
 overflow(); 
 
}
